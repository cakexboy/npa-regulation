<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\User;
use App\Entity\NpaProject;

class RegulationController extends AbstractController
{
    /**
     * @Route("/filldatabase", name="filldatabase")
     */
    public function filldb()
    {
    $entityManager = $this->getDoctrine()->getManager();
    $names_array = [
        1 => 'Abagail',
        2 => 'Abbey',
        3 => 'Evangeline',
        4 => 'Liam',
        5 => 'Madison',
        6 => 'Ruby',
        7 => 'William',
        8 => 'Grace',
    ];
    $titles_array = [
        1 =>'economy',
        2 =>'social services',
        3 =>'transport',
        4 =>'investments',
        5 =>'agriculture',
        6 =>'legal restrictions',
        7 =>'entrepreneurship',
        8 =>'cash deposits',
    ];
    for ($i = 1; $i <= 2; $i++) {
        $number = random_int(1,8);
        $name = $names_array[$number];
        
        $user = new User();
        $user->setName($name);
        $user->setEmail(sprintf('%s_@gmail.com',$name));
        $entityManager->persist($user);
        
        $npa = new NpaProject();
        $npa->setTitle($titles_array[$number]);
        $npa->setAuthor($name);
        $entityManager->persist($npa);
    }
    $entityManager->flush();
    return $this->render('regulation/index.html.twig', [
        'controller_name' => 'RegulationController',
    ]);
    }
    /**
     * @Route("/", name="all_projects")
     */
    public function npa_show()
    {
	$repository = $this->getDoctrine()->getRepository(NpaProject::class);
	$projects = [];

   	 for ($id = 1; $id <= count($repository->findAll()); $id++)
   	 {
        	$npa = $this->getDoctrine()
                	 ->getRepository(NpaProject::class)
                	 ->find($id);
       		$projects[] = $npa;
   	 }
   	 return $this->render('regulation/project.html.twig', [
         'projects' => $projects,
     ]);
    }
}
